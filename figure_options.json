{
    "title":
    {
        "label": "Title",
        "default": "My Graph",
        "type": "string"
    },

    "titleColor":
    {
        "label": "Title Color",
        "default": "#ccc",
        "type": "color"
    },

    "backgroundColor":
    {
        "label": "Background Color",
        "default": "#444",
        "type": "color"
    },

    "textColor":
    {
        "label": "Text Color",
        "default": "#ccc",
        "type": "color"
    },

    "showlegend":
    {
        "type": "boolean",
        "default": true,
        "label": "Show Legend",
        "description": "Whether or not a legend is drawn."
    },

    "margin": {

        "label": "Margins",

        "l":
        {
            "type": "int",
            "default": 80,
            "label": "Left",
            "description": "The left margin (in pixels.)"
        },

        "r":
        {
            "type": "int",
            "default": 80,
            "label": "Right",
            "description": "The right margin (in pixels.)"
        },

        "t":
        {
            "type": "int",
            "default": 100,
            "label": "Top",
            "description": "The top margin (in pixels.)"
        },

        "b":
        {
            "type": "int",
            "default": 80,
            "label": "Bottom",
            "description": "The bottom margin (in pixels.)"
        },

        "pad":
        {
            "type": "int",
            "default": 0,
            "label": "Padding",
            "description": "The amount of padding (in pixels) between the plotting area and the axis lines."
        }
    },

    "xaxis":
    {
        "label": "X Axis",

        "title":
        {
            "label": "Name",
            "default": "",
            "type": "string"
        },

        "type":
        {
            "label": "Type",
            "default": "detect",
            "type": ["detect", "linear", "log", "date", "category"],
            "description": "The axis type. By default, with 'detect', it attempts to determine the axis type by looking into the data of the groups that reference the axis in question."
        },

        "showline":
        {
            "label": "Show Line",
            "type": "boolean",
            "default": true,
            "description": "Whether or not the bounding line for the axis is drawn."
        },

        "linecolor":
        {
            "label": "Line Color",
            "default": "#222",
            "type": "color",
            "description": "The color of the axis line."
        },

        "linewidth":
        {
            "label": "Line Width",
            "default": 1,
            "type": "int",
            "description": "The width (in pixels) of the axis line."
        },

        "zeroline":
        {
            "label": "Zero Line",
            "default": false,
            "type": "boolean",
            "description": "Whether or not a line is drawn at the 0 value of the axis."
        },

        "zerolinecolor":
        {
            "label": "Zero Line Color",
            "default": "#333",
            "type": "color",
            "description": "The color of the line drawn at the 0 value."
        },

        "zerolinewidth":
        {
            "label": "Zero Line Width",
            "default": 1,
            "type": "int",
            "description": "The width of the line drawn at the 0 value."
        },

        "tickColor":
        {
            "label": "Tick Color",
            "default": "#333",
            "type": "color"
        },

        "minorTickColor":
        {
            "label": "Minor Tick Color",
            "default": "#383838",
            "type": "color"
        },

        "textColor":
        {
            "label": "Text Color",
            "default": "#ccc",
            "type": "color"
        },

        "range":
        {
            "label": "Range",

            "autorange":
            {
                "label": "Auto Range",
                "type": "boolean",
                "default": true,
                "description": "Whether or not the range of this axis is computed in relation to the input data. See 'Range Mode' for more info."
            },

            "rangemode":
            {
                "label": "Range Mode",
                "type": ["normal", "tozero", "nonnegative"],
                "default": "normal",
                "description": "**normal**: the range is computed in relation to the extrema of the input data.\n**tozero**: the range extends to 0 regardless of the input data.\n**nonnegative**: the range is non-negative, regardless of the input data."
            },

            "fixedrange":
            {
                "label": "Fixed Range",
                "type": "boolean",
                "default": false,
                "description": "Whether or not this axis is zoom-able. When true, zoom is disabled."
            },

            "start":
            {
                "label": "Start",
                "type": "float",
                "default": 0,
                "description": "The minimum value for the range of this axis."
            },

            "end":
            {
                "label": "End",
                "type": "float",
                "default": 0,
                "description": "The maximum value for the range of this axis."
            }
        },

        "grid":
        {
            "label": "Grid",

            "showgrid":
            {
                "label": "Show Grid",
                "default": false,
                "type": "boolean",
                "description": "Whether or not grid lines are drawn. If 'true' the grid lines are drawn at every tick mark."
            },

            "gridcolor":
            {
                "label": "Color",
                "default": "#eee",
                "type": "color",
                "description": "The color of the grid lines."
            },

            "gridwidth":
            {
                "label": "Width",
                "default": 1,
                "type": "int",
                "description": "The width of the grid lines (in pixels.)"
            }
        }
    },

    "yaxis":
    {
        "label": "Y Axis",

        "title":
        {
            "label": "Name",
            "default": "",
            "type": "string"
        },

        "type":
        {
            "label": "Type",
            "default": "detect",
            "type": ["detect", "linear", "log", "date", "category"],
            "description": "The axis type. By default, with 'detect', it attempts to determine the axis type by looking into the data of the groups that reference the axis in question."
        },

        "showline":
        {
            "label": "Show Line",
            "type": "boolean",
            "default": true,
            "description": "Whether or not the bounding line for the axis is drawn."
        },

        "linecolor":
        {
            "label": "Line Color",
            "default": "#222",
            "type": "color",
            "description": "The color of the axis line."
        },

        "linewidth":
        {
            "label": "Line Width",
            "default": 1,
            "type": "int",
            "description": "The width (in pixels) of the axis line."
        },

        "zeroline":
        {
            "label": "Zero Line",
            "default": true,
            "type": "boolean",
            "description": "Whether or not a line is drawn at the 0 value of the axis."
        },

        "zerolinecolor":
        {
            "label": "Zero Line Color",
            "default": "#333",
            "type": "color",
            "description": "The color of the line drawn at the 0 value."
        },

        "zerolinewidth":
        {
            "label": "Zero Line Width",
            "default": 1,
            "type": "int",
            "description": "The width of the line drawn at the 0 value."
        },

        "tickColor":
        {
            "label": "Tick Color",
            "default": "#333",
            "type": "color"
        },

        "minorTickColor":
        {
            "label": "Minor Tick Color",
            "default": "#383838",
            "type": "color"
        },

        "textColor":
        {
            "label": "Text Color",
            "default": "#ccc",
            "type": "color"
        },

        "range":
        {
            "label": "Range",

            "autorange":
            {
                "label": "Auto Range",
                "type": "boolean",
                "default": true,
                "description": "Whether or not the range of this axis is computed in relation to the input data. See 'Range Mode' for more info."
            },

            "rangemode":
            {
                "label": "Range Mode",
                "type": ["normal", "tozero", "nonnegative"],
                "default": "normal",
                "description": "**normal**: the range is computed in relation to the extrema of the input data.\n**tozero**: the range extends to 0 regardless of the input data.\n**nonnegative**: the range is non-negative, regardless of the input data."
            },

            "fixedrange":
            {
                "label": "Fixed Range",
                "type": "boolean",
                "default": false,
                "description": "Whether or not this axis is zoom-able. When true, zoom is disabled."
            },

            "start":
            {
                "label": "Start",
                "type": "float",
                "default": 0,
                "description": "The minimum value for the range of this axis."
            },

            "end":
            {
                "label": "End",
                "type": "float",
                "default": 0,
                "description": "The maximum value for the range of this axis."
            }
        },

        "grid":
        {
            "label": "Grid",

            "showgrid":
            {
                "label": "Show Grid",
                "default": false,
                "type": "boolean",
                "description": "Whether or not grid lines are drawn. If 'true' the grid lines are drawn at every tick mark."
            },

            "gridcolor":
            {
                "label": "Color",
                "default": "#eee",
                "type": "color",
                "description": "The color of the grid lines."
            },

            "gridwidth":
            {
                "label": "Width",
                "default": 1,
                "type": "int",
                "description": "The width of the grid lines (in pixels.)"
            }
        }
    }
}
