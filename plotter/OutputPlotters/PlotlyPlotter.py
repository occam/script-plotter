# from .AbstractPlotter import AbstractPlotter
from collections import OrderedDict

import json

# import plotly.graph_objs as go


class PlotlyPlotter:#( AbstractPlotter ):

    """
        Plot types:
            - scatter
            - bar
            - heatmap
            - histogram
            - histogram 2d
            - area
            - pie
            - contour
            - histogram 2d contour
    """

    def __init__(self):
        return;

    def plot( self, data, layout, output_dir_path="." ):
        plot_lines = []
        x_data=data.get("x")
        y_data=data.get("y")
        legend=data.get("legend")
        plot_type=data.get("type")
        for i in range(0,len(y_data)):
            if(len(x_data)==1):
                x=x_data[0]
            else:
                x=x_data[i]
            # trace = go.Scatter(
            #     x = x,
            #     y = y_data[i],
            #     name = legend[i]
            # )
            trace = {
                "type": plot_type,
                "x": x,
                "y": y_data[i],
                "name": legend[i]
            }
            plot_lines.append(trace)
        output_template = OrderedDict({
        	"data" : plot_lines,
        	"layout" : layout
        })
        with open("%s/%s.json"%(output_dir_path,layout.get("title").replace(" ", "_") ), 'w') as of:
            json.dump(output_template, of, sort_keys=True, indent=4)
        with open("%s/object.json"%(output_dir_path), 'w') as of:
            title = layout.get("title","no_title")
            json.dump({"file":"%s.json"%(title.replace(" ", "_")), "name":"%s"%(title) }, of, sort_keys=True, indent=4)
        return;
