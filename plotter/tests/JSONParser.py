from plotter.InputParsers.JSONParser import JSONParser


parsed_json={
    "data_name": "XSim for different memory frequencies",
    "experiments": [
        {
            "registers": [
                {
                    "r4": 24,
                    "r5": 14,
                    "r6": 20,
                    "r7": 30,
                    "r0": 10,
                    "r1": 100,
                    "r2": 10,
                    "r3": 100
                }
            ],
            "stats": [
                {
                    "jalr": 1,
                    "sw": 1,
                    "jr": 1,
                    "lw": 1,
                    "lis": 6,
                    "put": 2,
                    "instructions": 13,
                    "cycles": 20009,
                    "halt": 1
                }
            ],
            "clock_frequency": "200Hz"
        },
        {
            "registers": [
                {
                    "r4": 24,
                    "r5": 14,
                    "r6": 20,
                    "r7": 30,
                    "r0": 10,
                    "r1": 100,
                    "r2": 10,
                    "r3": 100
                }
            ],
            "stats": [
                {
                    "jalr": 1,
                    "sw": 1,
                    "jr": 1,
                    "lw": 1,
                    "lis": 6,
                    "put": 2,
                    "instructions": 13,
                    "cycles": 29,
                    "halt": 1
                }
            ],
            "clock_frequency": "200KHz"
        },
        {
            "registers": [
                {
                    "r4": 24,
                    "r5": 14,
                    "r6": 20,
                    "r7": 30,
                    "r0": 10,
                    "r1": 100,
                    "r2": 10,
                    "r3": 100
                }
            ],
            "stats": [
                {
                    "jalr": 1,
                    "sw": 1,
                    "jr": 1,
                    "lw": 1,
                    "lis": 6,
                    "put": 2,
                    "instructions": 13,
                    "cycles": 15,
                    "halt": 1
                }
            ],
            "clock_frequency": "200MHz"
        }
    ]
}

# JSONParser.convert_ranges("experiment(:).data_y(0:end-1)",parsed_json)
# JSONParser.convert_ranges("experiment(:).setting(:).output.data_y(0:end-1)",parsed_json)

parser=JSONParser()
parser.parsed_json=parsed_json
x_data, y_data, names = parser.get("experiments(:).clock_frequency",
"experiments(:).stats(0).cycles",
"data_name")
print(x_data)
print(y_data)
print(names)


y_path = "experiments(:).stats(0)"#".cycles"
y_data = JSONParser.convert_ranges(y_path, parsed_json)
print(y_data)

x_path = "experiments(:).clock_frequency"
x_data = JSONParser.convert_ranges(x_path, parsed_json)
print(x_data)

names_path = "experiments(:).clock_frequency"
names = JSONParser.convert_ranges(names_path, parsed_json)
print(names)
