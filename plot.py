from plotter.InputParsers.CSVParser import CSVParser as CSVParser;
from plotter.InputParsers.JSONParser import JSONParser as JSONParser;
import pdb;
from plotter.OutputPlotters.PlotlyPlotter import PlotlyPlotter;
from copy import deepcopy
import json
import sys
from plotter.common.utils import parse_range
def plot(inputs_configurations_path="", figure_configurations_path="", output_dir_path="", input_files_path=[""], n_inputs=1):


    ## If outside of OCCAM these will be empty, fill them
    if(output_dir_path==""):
        output_dir_path='.'


    if(inputs_configurations_path == ""):
        inputs_configurations={
            "csv": {
                "custom_separator": ",",
                "custom_quotechar": '"',
                "x_data": "0",
                "data_lines": "1:1:end",
                "y_data": "1:1:end",
                "data_variable_names": 0,
                "separator": ",",
                "quotechar": "\"",
                "advanced": "Hide"
            },
            "json": {
                "x_data": "experiments(:).x_data",
                "y_data": "experiments(:).y_data",
                "data_variable_names": "experiments(:).legend",
            },
            "type": "JSON",
            "plot": "scatter"
        }
    else:
        with open(inputs_configurations_path) as json_file:
            inputs_configurations=json.load(json_file)



    if(figure_configurations_path == ""):
        figure_configurations={
            "titleColor": "#ccc",
            "textColor": "#ccc",
            "showlegend": True,
            "margin": {
                "t": 100,
                "r": 80,
                "l": 80,
                "b": 80,
                "pad": 0
            },
            "title": "My Graph",
            "backgroundColor": "#444",
            "yaxis": {
                "tickColor": "#333",
                "zeroline": True,
                "range": {
                    "start": 0.0,
                    "fixedrange": False,
                    "autorange": True,
                    "end": 0.0,
                    "rangemode": "normal"
                },
                "zerolinecolor": "#333",
                "minorTickColor": "#383838",
                "linecolor": "#222",
                "linewidth": 1,
                "type": "detect",
                "showline": True,
                "textColor": "#ccc",
                "grid": {
                    "gridwidth": 1,
                    "gridcolor": "#eee",
                    "showgrid": False
                },
                "zerolinewidth": 1,
                "title": ""
            },
            "xaxis": {
                "tickColor": "#333",
                "zeroline": False,
                "range": {
                    "start": 0.0,
                    "fixedrange": False,
                    "autorange": True,
                    "end": 0.0,
                    "rangemode": "normal"
                },
                "zerolinecolor": "#333",
                "minorTickColor": "#383838",
                "linecolor": "#222",
                "linewidth": 1,
                "type": "detect",
                "showline": True,
                "textColor": "#ccc",
                "grid": {
                    "gridwidth": 1,
                    "gridcolor": "#eee",
                    "showgrid": False
                },
                "zerolinewidth": 1,
                "title": ""
            }
        }
    else:
        with open(figure_configurations_path) as json_file:
            figure_configurations=json.load(json_file)

    ## Do stuff to figure out the type of input
    parser = None
    data_x = []
    data_y = []
    data_legend=[]

    for i in range(0,n_inputs):
        input_file_path=input_files_path[i]
        if(input_file_path == ""):
            if inputs_configurations.get("type") == "CSV":
                input_file_path='./example_input.csv'
            elif inputs_configurations.get("type") == "JSON":
                input_file_path='./example_input2.json'

        #IF it is a CSV
        # parse it
        if(inputs_configurations.get("type")=="CSV"):

            csv_configurations = inputs_configurations.get("csv")

            separator = csv_configurations.get("separator")
            if(separator.lower() == "user defined"):
                separator = csv_configurations.get("custom_separator")

            quotechar = csv_configurations.get("quotechar")
            if(quotechar.lower() == "user defined"):
                quotechar = csv_configurations.get("custom_quotechar")

            parser = CSVParser()
            parsed_csv=parser.parse(
                input_file = input_file_path,
                options={"delimiter":separator,"quotechar":quotechar}
            )

            n_lines = len(parsed_csv)
            n_columns = 0
            if(n_lines>0):
                n_columns = len(parsed_csv[0])

            data_lines = parse_range(
                csv_configurations.get("data_lines"),
                n_lines
            )

            data_variable_names = parse_range(
                csv_configurations.get("data_variable_names"),
                n_lines
            )

            x_data_columns = parse_range(
                csv_configurations.get("x_data"),
                n_columns
            )

            y_data_columns = parse_range(
                csv_configurations.get("y_data"),
                n_columns
            )

            #TODO: If sizeof data_x != sizeof data_y something is wrong, unless sizeof data_x == 1
            if (len(x_data_columns)!=1 and (len(x_data_columns)!=len(y_data_columns))):
                print("Something is wrong with the size of x and y data")


            for col in x_data_columns:
                data_col=[]
                for row in data_lines:
                    data_col.append(parsed_csv[row][col])
                data_x.append(deepcopy(data_col))

            for col in y_data_columns:
                data_col=[]
                for row in data_lines:
                    data_col.append(parsed_csv[row][col])
                data_y.append(deepcopy(data_col))

            for col in y_data_columns:
                for row in data_variable_names:
                    data_legend.append(parsed_csv[row][col])

        elif(inputs_configurations.get("type")=="JSON"):
            json_configurations = inputs_configurations.get("json")
            variable_path = json_configurations.get("data_variable_names")
            x_path = json_configurations.get("x_data")
            y_path  = json_configurations.get("y_data")

            parser = JSONParser()
            parsed_json=parser.parse(
                input_file = input_file_path,
                options={}
            )
            t_data_x, t_data_y, t_data_legend = parser.get(x_path, y_path, variable_path)
            data_x.extend(t_data_x)
            data_y.extend(t_data_y)
            data_legend.extend(t_data_legend)
    data={
        "type": inputs_configurations.get("plot","scatter"),
        "x":data_x,
        "y":data_y,
        "legend":data_legend
    }
    plotter=PlotlyPlotter();
    plotter.plot(data, figure_configurations,output_dir_path)




if (len(sys.argv)>=6):
    inputs = []
    for i in range(5, 5+int(sys.argv[4])):
        inputs.append(sys.argv[i])
    plot(inputs_configurations_path=sys.argv[1], figure_configurations_path=sys.argv[2], output_dir_path=sys.argv[3], input_files_path=inputs, n_inputs=int(sys.argv[4]))
else:
    plot()
